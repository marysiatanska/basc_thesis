# all dimensions in cm

GROUND_FLOOR_HEIGHT = 510.
REGULAR_FLOOR_HEIGHT = 340.
LAST_FLOOR_HEIGHT = 510.

SEGMENT_WIDTH = 1830
SEGMENT_DEPTH = 1260

SEGMENT_AXES_X = 0, 630, 1200, 1830
SEGMENT_AXES_Y = 0, 630, 1260

FlATS_BY_FLOOR_TYPE = {
    'A': [],
    'B': [],
    'C': [],
    'D': [],
    'E': [],
    'F': [],
    'G': [],
    'H': [],
    'I': [],
    'J': [],
    'K': []
}

INCORRECT_ROTATION_MODES = {3, 4, 5}